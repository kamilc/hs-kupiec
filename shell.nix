{ nixpkgs ? import <nixpkgs> { },
  ghc ? nixpkgs.ghc,
  startPostgres ? false,
  stopPostgres ? false,
  initPostgresDatabase ? " ",
  createPostgresDatabase ? " ",
  postgresPort ? "53001"
}:

with nixpkgs;

let
  _startPostgres = if startPostgres then "true" else "false";
  _stopPostgres = if stopPostgres then "true" else "false";
in
  haskell.lib.buildStackProject {
    inherit ghc;
    name = "hs-kupiec";
    buildInputs = [
      postgresql100
      zlib
      blas
      liblapack
      gfortran
      gfortran.cc
      haskellPackages.hoogle
      ncurses
    ];
    shellHook = ''
      source .env

      if [ '${initPostgresDatabase}' != ' ' ]; then
        echo "Initializing database"
        initdb ${initPostgresDatabase}
      fi

      if [ '${_stopPostgres}' = 'true' ]; then
        echo "Stopping Postgres"
        pg_ctl -D postgres stop
      fi

      if [ '${_startPostgres}' = 'true' ]; then
        echo "Starting Postgres"
        pg_ctl -D postgres -l log/postgres.log -o "-p ${postgresPort}" start
      fi

      if [ '${createPostgresDatabase}' != ' ' ]; then
        echo "Creating database"
        createdb -p ${postgresPort} ${createPostgresDatabase}
      fi
    '';
  }
