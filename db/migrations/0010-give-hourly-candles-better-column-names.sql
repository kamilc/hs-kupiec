drop materialized view hourly_candles;

create materialized view hourly_candles as (
  with recursive hours as (
    select date_trunc('hour', now()) as h,
           date_trunc('hour', now()) as start
    union
    select hours.h - '1 hour' :: interval, hours.start
    from hours
    where hours.h > hours.start - '365 days' :: interval
  )
  select
    hours.h as hour,
    symbols.code as symbol__code,
    max(high) as high,
    min(low) as low,
    min(open) as open,
    max(close) as close
  from hours
  inner join symbols on true
  left outer join (
    select max(high) over (partition by date_trunc('hour', to_timestamp(epoch)), symbol__code) as high,
           min(low) over (partition by date_trunc('hour', to_timestamp(epoch)), symbol__code) as low,
           first_value(open) over (partition by date_trunc('hour', to_timestamp(epoch)), symbol__code order by epoch asc) as open,
           first_value(close) over (partition by date_trunc('hour', to_timestamp(epoch)), symbol__code order by epoch desc) as close,
           date_trunc('hour', to_timestamp(epoch)) as epoch,
           symbol__code
    from candles
  ) stats on stats.epoch = hours.h and stats.symbol__code = symbols.code
  group by hours.h, symbols.code
);



