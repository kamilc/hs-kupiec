create table candles (
    epoch bigint not null,
    open numeric(20,10) not null,
    high numeric(20,10) not null,
    low numeric(20,10) not null,
    close numeric(20,10) not null,
    symbol character varying references symbols ( code )
);

