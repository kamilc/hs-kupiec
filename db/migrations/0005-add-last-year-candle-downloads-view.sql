create view last_year_candle_downloads as (
  with recursive epochs as (
    select round(extract(epoch from now())) as epoch,
           round(extract(epoch from now())) as start
    union
    select epochs.epoch - 86400, epochs.start
    from epochs
    where epochs.epoch > epochs.start - 365 * 86400
  )
  select epochs.epoch,
         symbols.code
  from epochs
  join symbols on true
  left outer join candles on candles.epoch = epochs.epoch
  where candles.open is null
);

