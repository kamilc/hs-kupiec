create table symbols (
    code character varying primary key,
    allow_forward_starting boolean,
    type character varying,
    display_name character varying NOT NULL,
    is_trading_suspended boolean NOT NULL,
    exchange_is_open boolean,
    market character varying,
    submarket character varying,
    pip numeric(10,10) NOT NULL
);
