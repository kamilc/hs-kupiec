{-# LANGUAGE OverloadedStrings #-}

module Config (
                Config(..)
              , loadConfig
              ) where

import System.Environment
import Data.Maybe
import           Katip
import           Data.Text
import qualified Configuration.Dotenv as Dotenv

data Config = Config {
                       _binaryAppId :: String
                     , _binaryApiToken :: String
                     , _binaryApiUrl :: String
                     , _binaryApiPath :: String
                     , _databaseConnectionString :: String
                     , _loggingFileName :: String
                     , _configEnv :: String
                     , _logEnv :: LogEnv
                     }

loadConfig :: IO Config
loadConfig = do
    Dotenv.parseFile "./.env" >>= Dotenv.load True

    binaryAppId <- require "KUPIEC_BINARY_API_ID"
    binaryApiToken <- require "KUPIEC_BINARY_API_TOKEN"
    binaryApiUrl <- require "KUPIEC_BINARY_API_URL"
    binaryApiPath <- require "KUPIEC_BINARY_API_PATH"
    databaseConnectionString <- require "KUPIEC_DATABASE_CONNECTION_STRING"
    loggingFileName <- optional "KUPIEC_LOG_FILE_NAME" "log/kupiec.log"
    env <- optional "KUPIEC_ENV" "development"

    handleScribe <- mkFileScribe "./log/kupiec.log" InfoS V2
    let makeLogEnv = registerScribe "file" handleScribe defaultScribeSettings =<< initLogEnv "hs-kupiec" (environment env)
    logEnv <- makeLogEnv

    return $ Config {
                      _binaryAppId = binaryAppId
                    , _binaryApiToken = binaryApiToken
                    , _binaryApiUrl = binaryApiUrl
                    , _binaryApiPath = binaryApiPath
                    , _configEnv = env
                    , _databaseConnectionString = databaseConnectionString
                    , _loggingFileName = loggingFileName
                    , _logEnv = logEnv
                    }
  where
    require key = getEnv key
    optional key def = lookupEnv key >>= return . fromMaybe def
    environment str = Environment { Katip.getEnvironment = pack str }
