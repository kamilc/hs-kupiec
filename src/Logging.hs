{-# LANGUAGE OverloadedStrings #-}

module Logging where

import           Katip

debug :: Katip m => String -> m ()
debug msg =
  logF () "" DebugS $ logStr msg

info :: Katip m => String -> m ()
info msg =
  logF () "" InfoS $ logStr msg

warning :: Katip m => String -> m ()
warning msg =
  logF () "" WarningS $ logStr msg

fatal :: Katip m => String -> m ()
fatal msg =
  logF () "" ErrorS $ logStr msg
