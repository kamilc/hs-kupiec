{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FunctionalDependencies #-}

module BinaryApi.Core (
                   BinaryApiContext(..)
                 , ApiMessage (..)
                 , createBinaryApiContext
                 , BinaryApi(..)
                 , ApiError(..)
                 ) where

import           Data.Text
import           Control.Concurrent
import           Control.Exception
import           Control.Monad       (forever)
import           Control.Concurrent.STM
import           Control.Concurrent.MVar
import qualified Network.WebSockets as WS
import           Data.ByteString.Lazy (toStrict)
import           Network.WebSockets.Stream (makeStream)
import           Network.Connection
import           Wuss
import qualified Data.ByteString.Lazy as BL
import           Data.Aeson
import           Data.Aeson.Types
import           Data.Maybe
import           Data.Either
import qualified Data.HashMap.Strict as HMS
import           GHC.Generics
import           Conversions
import           Control.Monad.IO.Class
import           Katip

newtype BinaryAction a = BinaryAction {
                                        _runBinaryAction :: KatipT IO a
                                      } deriving (Monad, Functor, Applicative, Katip, MonadIO)

runBinaryAction :: LogEnv -> BinaryAction a -> IO a
runBinaryAction env action = runKatipT env $ _runBinaryAction action

class BinaryApi a b | a -> b where
    parseResponse :: FromJSON b => a -> Value -> Parser b
    parseResponse _ = parseJSON

    request :: (ToJSON a, FromJSON b) => BinaryApiContext -> a -> IO (Either ApiError b)
    request context request = do
        let env = _binaryApiContextEnvironment context;
            events = _binaryApiContextEvents context
         in runBinaryAction env $ do
              resp <- liftIO $ composeMessage >>= enqueueMessage >>= waitMessageResponse
              liftIO $ debug events $ "Decoding message: " ++ (show resp)
              return $ decodeMessage resp
      where
        composeMessage = do
            nextId <- atomically $ do
                _id <- readTVar lastRequestIdTV
                writeTVar lastRequestIdTV $ _id + 1
                return $ _id + 1
            return $ ApiMessage nextId (toJSON request)
          where
            lastRequestIdTV = _binaryApiContextLastRequestId context

        enqueueMessage message = do
            atomically $ writeTChan requestsChan message
            return message

        waitMessageResponse request@(ApiMessage id _) = do
            message <- atomically $ do
                candidate <- readTChan responsesChan
                case candidate of
                    (ApiMessage _id _) | _id == id -> return $ Just candidate
                    _ -> do
                        unGetTChan responsesChan candidate
                        return Nothing
            case message of
                Nothing -> do
                    threadDelay 10
                    waitMessageResponse request
                Just response -> return response

        decodeMessage (ApiMessage id body@(Object hashMap)) =
            case HMS.lookup "error" hashMap of
                Just (Object err) ->
                    case HMS.lookup "code" err of
                        Just "StreamingNotAllowed" -> Left $ ApiStreamingNotAllowed
                        Just (Data.Aeson.String other) -> case HMS.lookup "message" err of
                                        Just (Data.Aeson.String msg) -> Left $ ApiError other msg
                                        _ -> Left $ ApiMalformedResponse "No usual error code and message" body
                        _ -> Left $ ApiMalformedResponse "No usual error code and message" body
                Just _ -> Left $ ApiMalformedResponse "No usual error code and message" body
                Nothing ->
                    case parse (parseResponse request) body of
                          Success decoded -> Right decoded
                          Error msg -> Left $ ApiMalformedResponse msg body

        requestsChan = _binaryApiContextRequests context
        responsesChan = _binaryApiContextResponses context

data ApiError = ApiError Text Text | ApiStreamingNotAllowed | ApiMalformedResponse String Value
  deriving (Generic, Show)

instance FromJSON ApiError where
    parseJSON = withObject "ApiError" $ \v ->
        ApiError <$> v .: "code" <*> v .: "message"

data ApiMessage = ApiMessage Int Value

instance Show ApiMessage where
    show (ApiMessage id entity) = "ApiMessage " ++ (show id) ++ " " ++ (show entity)

instance ToJSON ApiMessage where
    toJSON (ApiMessage id payload) = addId $ toJSON payload
      where
        addId (Object hashMap) = Object $ HMS.insert "req_id" (toJSON id) hashMap

instance FromJSON ApiMessage where
    parseJSON = withObject "ApiMessage" $ \v ->
      ApiMessage <$> v .: "req_id" <*> (pure $ Object v)

instance WS.WebSocketsData ApiMessage where
    fromLazyByteString = fromJust . decode
    toLazyByteString = encode

data BinaryApiContext = BinaryApiContextT {
                                            _binaryApiContextRequests :: TChan ApiMessage
                                          , _binaryApiContextResponses :: TChan ApiMessage
                                          , _binaryApiContextEvents :: TChan (Severity, LogStr)
                                          , _binaryApiContextLastRequestId :: TVar Int
                                          , _binaryApiContextEnvironment :: LogEnv
                                          }

runSecureClientNoValidate :: String -> String -> (WS.Connection -> IO ()) -> IO ()
runSecureClientNoValidate host path action = do
    let port = 443
    let options = WS.defaultConnectionOptions
    let headers = []
    let tlsSettings = TLSSettingsSimple {
          settingDisableCertificateValidation = True
        , settingDisableSession = False
        , settingUseServerName = False
        }
    let connectionParams = ConnectionParams {
          connectionHostname = host
        , connectionPort = port
        , connectionUseSecure = Just tlsSettings
        , connectionUseSocks = Nothing
        }

    context <- initConnectionContext
    connection <- connectTo context connectionParams
    stream <- makeStream
        (fmap Just (connectionGetChunk connection))
        (maybe (return ()) (connectionPut connection . toStrict))
    WS.runClientWithStream stream host path options headers action

createBinaryApiContext :: String -> String -> String -> LogEnv -> IO BinaryApiContext
createBinaryApiContext url path token env = do
    requestsChannel <- atomically $ newBroadcastTChan
    responsesChannel <- atomically $ newTChan
    eventsChannel <- atomically $ newTChan
    lastId <- atomically $ newTVar 0
    socketInitialized <- newEmptyMVar

    forkIO $ runBinarySocket requestsChannel responsesChannel eventsChannel socketInitialized

    -- let's wait for the forked code to fill in the mvar signalling
    -- that it started listening
    readMVar socketInitialized

    return BinaryApiContextT {
                               _binaryApiContextRequests = requestsChannel
                             , _binaryApiContextResponses = responsesChannel
                             , _binaryApiContextEvents = eventsChannel
                             , _binaryApiContextLastRequestId = lastId
                             , _binaryApiContextEnvironment = env
                             }
  where
    runBinarySocket :: TChan ApiMessage -> TChan ApiMessage -> TChan (Severity, LogStr) -> MVar Bool -> IO ()
    runBinarySocket requests responses events socketInitialized = do
        catch connectWithApi retryConnect
      where
        connectWithApi = do
            putStrLn $ "Trying to connect on " ++ url ++ path
            runSecureClientNoValidate url path $ forkBinaryWorkers requests responses events socketInitialized
        retryConnect :: IOException -> IO ()
        retryConnect e = do
          putStrLn $ show e
          runBinarySocket requests responses events socketInitialized

debug :: TChan (Severity, LogStr) -> String -> IO ()
debug chan msg =
  atomically $ writeTChan chan (DebugS, logStr msg)

info :: TChan (Severity, LogStr) -> String -> IO ()
info chan msg =
  atomically $ writeTChan chan (InfoS, logStr msg)

warning :: TChan (Severity, LogStr) -> String -> IO ()
warning chan msg =
  atomically $ writeTChan chan (WarningS, logStr msg)

fatal :: TChan (Severity, LogStr) -> String -> IO ()
fatal chan msg =
  atomically $ writeTChan chan (ErrorS, logStr msg)

forkBinaryWorkers :: TChan ApiMessage -> TChan ApiMessage -> TChan (Severity, LogStr) -> (MVar Bool) -> WS.ClientApp ()
forkBinaryWorkers requests responses events socketInitialized connection = do
    putStrLn "Connected with Binary API"

    forkIO $ loopReadData

    -- the following thread will get killed when the connection
    -- will get terminated:
    WS.forkPingThread connection 30

    forkIO $ do
      threadDelay 50000
      isEmpty <- isEmptyMVar socketInitialized
      if isEmpty then
        putMVar socketInitialized True
      else return ()

    loopWriteData
  where
    loopWriteData = do
        reqs <- atomically $ dupTChan requests
        forever $ do
            info events "Listening for next request"
            request <- atomically $ readTChan reqs
            info events $ "Sending to binary.com: " ++ (show $ encode request)
            WS.sendTextData connection request
    loopReadData = do
        forever $ do
            info events "Listening for next binary.com response"
            message <- WS.receiveData connection
            info events $ "Received from binary.com: " ++ (show message)
            atomically $ writeTChan responses message


