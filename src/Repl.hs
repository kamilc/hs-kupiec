module Repl (runRepl) where

import           System.Console.Haskeline
import           System.Console.Command
import           System.Console.Program
import           Data.Monoid.Colorful
import           Control.Monad.Reader

import           App
import           Config
import           Logging
import           Repl.Core
import           Repl.Download

runRepl :: App ()
runRepl = repl commands

commands :: Commands App
commands = Node root rootCommands
  where
    rootCommands = [
                     downloadCommands
                   , Node help []
                   ]

root :: Command App
root = command "kupiec" "Kupiec AI for binary.com trading" doHelp

help :: Command App
help = command "help" "Show usage info" doHelp

doHelp :: Action App
doHelp = io $ liftIO (showUsage commands)
