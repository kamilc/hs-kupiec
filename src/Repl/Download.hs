{-# LANGUAGE OverloadedStrings #-}

module Repl.Download where

import           System.Console.Haskeline
import           System.Console.Command
import           System.Console.Program
import           Data.Monoid.Colorful
import           Data.Either
import           Control.Monad.Reader
import           Control.Lens
import           App
import           Config
import           Logging
import           Repl.Core
import           BinaryApi
import           Database
import           Types

downloadCommands :: Commands App
downloadCommands = Node download [
                                   Node symbols []
                                 , Node candles []
                                 ]

withEitherResult :: Show e => Either e a -> (a -> App ()) -> App ()
withEitherResult e handleResult = either handleError handleResult e
  where
    handleError = liftIO . putStrLn . show

download :: Command App
download = command "download" "Fetches the data from binary.com into a local PostgreSQL" $ io printNoInputGiven

symbols :: Command App
symbols = command "symbols" "Downloads all symbols" $ io $ do
   esymbols <- requestBinary defaultActiveSymbolsRequest
   withEitherResult esymbols $ \(ActiveSymbolsResponse symbols) -> do
      info $ "Inserting symbols: " ++ (show symbols)
      enum <- runInsert $
        insert (kupiecDb ^. kupiecSymbols) (insertValues symbols) handleOnConflict
      withEitherResult enum $ \num -> do
        liftIO $ putStrLn $ "OK - persisted " ++ (show num) ++ " records"
  where
    handleOnConflict = onConflict (conflictingFields primaryKey) onConflictSetAll

candles :: Command App
candles = command "candles" "Downloads ticks history for existing in db symbols" $ io $ do
    edownloads <- runSelect $ select $ all_ (kupiecDb ^. kupiecLastYearCandleDownloads)
    withEitherResult edownloads $ \downloads -> do
      mapM_ runCandleDownload downloads
  where
    runCandleDownload :: LastYearCandleDownloadT Identity -> App ()
    runCandleDownload downloadSpec = do
      let code = downloadSpec ^. lastYearCandleDownloadCode;
          end  = downloadSpec ^. lastYearCandleDownloadEpoch;
          start = end - 86400;
          withCode = historyLogsRequestTicksHistory .~ code $ defaultHistoryLogsRequest;
          withEnd = historyLogsRequestEnd .~ end $ withCode;
          request = historyLogsRequestStart .~ start $ withEnd
       in do
         ecandles <- requestBinary request
         case ecandles of
           Left ApiStreamingNotAllowed -> do
             enum <- runInsert $
               let candle = defaultInvalidCandle {
                     _candleEpoch = (downloadSpec ^. lastYearCandleDownloadEpoch)
                   , _candleSymbol = SymbolId (downloadSpec ^. lastYearCandleDownloadCode)
                   };
                   handleOnConflict = onConflict (conflictingFields primaryKey) onConflictSetAll
                in insert (kupiecDb ^. kupiecCandles) (insertValues [ candle ]) handleOnConflict
             liftIO $ putStrLn $ "Got 'streaming not allowed' for: " ++ (show downloadSpec)
           Left _ -> liftIO $ putStrLn $ "Got an odd looking response for " ++ (show downloadSpec)
           Right (HistoryLogsResponse candles) -> do
             enum <- runInsert $
               let handleOnConflict = onConflict (conflictingFields primaryKey) onConflictSetAll
                in insert (kupiecDb ^. kupiecCandles) (insertValues candles) handleOnConflict
             case enum of
               Left err -> fail $ show err
               Right num -> liftIO $ putStrLn $ "Persisted " ++ (show num) ++ " for " ++ (show downloadSpec)

