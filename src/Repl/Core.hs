{-# LANGUAGE OverloadedStrings #-}

module Repl.Core (repl, printNoInputGiven) where

import           System.Console.Haskeline
import           System.Console.Haskeline.MonadException
import           System.Console.Program
import           System.Console.Command
import           Control.Monad.Reader
import           Data.Monoid.Colorful
import           App
import           Config
import           Logging

printNoInputGiven :: App ()
printNoInputGiven = liftIO $ putStrLn "No input given"

repl :: Commands App -> App ()
repl commands = do
    catch runPrompt handleError
  where
      handleError :: SomeException -> App ()
      handleError e = do
          liftIO $ putStrLn "Ooops.. Something weird has happened"
          repl commands
      runPrompt = interactiveWithPrompt prompt commands
      prompt = do
          term <- liftIO $ getTerm
          parts <- sequence [ mainName, envName, promptArrow ]
          let builders = map (showColoredS term) parts
              buildList = foldr (.) ((++) "") builders
           in return $ concat $ map (\b -> b "") builders

      mainName = return $ Fg Green "kupiec "
      envName = do
          env <- ask >>= return . _configEnv
          return $ (Fg Magenta) . Value $ "(" ++ env ++ ") "
      promptArrow = return $ Fg Green " ~> "


