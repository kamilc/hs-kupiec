module Conversions where

import           Data.Text hiding (replace, toLower, isUpper)
import           Data.Char (isUpper, toLower)
import           Data.Aeson
import           Data.List.Utils (replace)

-- |
-- >>> toCamelCase "_apiMessage" "_apiMessageId"
-- "id"
-- >>> toCamelCase "_historyLogsRequest" "_historyLogsRequestTicksHistory"
-- "ticks_history"
--
toCamelCase :: [Char] -> [Char] -> [Char]
toCamelCase prefix label = switchCases $ replace prefix "" label
  where
    switchCases [] = ""
    switchCases chars = Prelude.drop 1 $ go "" chars
    go acc [] = acc
    go acc (x:xs) = let s = switchChar x acc in s `seq` go (acc ++ s) xs
    switchChar x acc =
      case isUpper x of
        True -> ['_', toLower x]
        False -> [ toLower x ]

defaultJsonOptions :: String -> Options
defaultJsonOptions prefix = defaultOptions {
                                             fieldLabelModifier = toCamelCase prefix
                                           }
