module Database.Core where

import           Data.Pool
import           Data.ByteString
import           Data.ByteString.UTF8 as UTF8
import           Database.Beam.Postgres

data DatabaseContext =
  DatabaseContext
  { _pool :: Pool Connection
  }

createDatabaseContext :: String -> IO DatabaseContext
createDatabaseContext connString = do
    pool <- createConnectionPool
    return DatabaseContext { _pool = pool }
  where
    createConnectionPool = createPool (connectPostgreSQL $ UTF8.fromString connString) close 2 30 2

getDatabaseContextPool :: DatabaseContext -> Pool Connection
getDatabaseContextPool = _pool

