{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleContexts #-}

module App where

import           Control.Applicative
import           Control.Monad.Reader
import           Control.Monad.Writer
import           Control.Monad.State
import           Control.Concurrent.STM
import           Control.Concurrent
import           Database.PostgreSQL.Simple
import           Data.Int
import           Data.Text
import           Data.Pool
import           Data.Aeson
import           Katip
import           System.Console.Haskeline
import           Data.Monoid.Colorful

import           Config
import           Database.Core
import           Database.Beam.Postgres
import           Database.Beam.Backend.SQL.SQL92
import           Database.Beam.Backend.Types
import qualified Database.Beam.Postgres.Conduit as PC
import           Database.Beam.Query
import           BinaryApi
import           Types

data AppState = AppState {
                            _databaseContext :: DatabaseContext
                          , _binaryApiContext :: BinaryApiContext
                          }

newtype App a = App {
                      _runApp :: ReaderT Config (StateT AppState (InputT IO)) a
                    } deriving (Monad, Applicative, Functor, MonadReader Config, MonadState AppState, MonadIO)

instance Katip App where
    getLogEnv = do
        config <- ask
        return $ _logEnv config

    localLogEnv fn action = do
        config <- ask
        let mod = \c -> c { _logEnv = (fn $ _logEnv c) }
         in local mod action

instance MonadException m => MonadException (StateT s m) where
    controlIO f = StateT $ \s -> controlIO $ \(RunIO run) -> let
                    run' = RunIO (fmap (StateT . const) . run . flip runStateT s)
                    in fmap (flip runStateT s) $ f run'

instance MonadException App where
  controlIO f = do
      state <- get
      join $ liftIO $ f $ RunIO $ \app -> do
          return app

createAppState :: Config -> IO AppState
createAppState config = do
    dbContext <- createDatabaseContext $ _databaseConnectionString config
    binaryApiContext <- createBinaryApiContext binaryUrl binaryPath binaryToken (_logEnv config)

    return AppState {
                       _databaseContext = dbContext
                     , _binaryApiContext = binaryApiContext
                     }
  where
    binaryUrl = _binaryApiUrl config
    binaryPath = _binaryApiPath config
    binaryToken = _binaryApiToken config

logEvents :: TChan (Severity, LogStr) -> App ThreadId
logEvents chan = do
    state <- get
    config <- ask
    liftIO $ forkIO $ forever $ do
        (severity, message) <- atomically $ readTChan chan
        runAppWithState state config $ logF () "" severity message

runApp :: App a -> IO a
runApp app = do
    appConfig <- loadConfig
    appState <- createAppState appConfig
    let events = _binaryApiContextEvents . _binaryApiContext $ appState
    (result, state) <- runAppWithState appState appConfig $ appWithChanneledLogging events
    cleanup state
    return result
  where
    cleanup state = putStrLn "cleanup..."
    appWithChanneledLogging events = logEvents events >> app

runAppWithState :: AppState -> Config -> App a -> IO (a, AppState)
runAppWithState appState appConfig app = do
    runInputT inputSettings (runStateT (runReaderT (_runApp app) appConfig) appState)
  where
    inputSettings = defaultSettings { historyFile = Just ".kupiec.history" }

-- | Database related

askDatabaseConnectionPool :: App (Pool Connection)
askDatabaseConnectionPool = do
    state <- get
    return $ getDatabaseContextPool $ _databaseContext state

runSelect :: FromBackendRow Postgres a => SqlSelect PgSelectSyntax a -> App (Either SqlError [a])
runSelect query = do
  pool <- askDatabaseConnectionPool
  liftIO $ withResource pool $ \conn ->
    liftIO $ catch (runSelect' conn) handleError
  where
    runSelect' conn = runBeamPostgres conn (runSelectReturningList query) >>= return . Right
    handleError :: SqlError -> IO (Either SqlError [a])
    handleError = return . Left

runInsert :: SqlInsert PgInsertSyntax -> App (Either SqlError Int64)
runInsert stmt = do
  pool <- askDatabaseConnectionPool
  liftIO $ withResource pool $ \conn ->
    liftIO $ catch (runInsert' conn) handleError
  where
    runInsert' conn = PC.runInsert conn stmt >>= return . Right
    handleError :: SqlError -> IO (Either SqlError Int64)
    handleError = return . Left

-- | BinaryAPI related

requestBinary :: (ToJSON a, FromJSON b, BinaryApi a b) => a -> App (Either ApiError b)
requestBinary req = do
    binaryContext <- get >>= return . _binaryApiContext
    logF () "binary" InfoS "Requesting binary"
    liftIO $ request binaryContext req

