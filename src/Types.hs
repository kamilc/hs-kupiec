{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE OverlappingInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Types where

import           Database.Beam
import           Database.Beam.Backend.SQL
import           Database.Beam.Postgres
import           Database.PostgreSQL.Simple.ToField
import           Database.PostgreSQL.Simple.FromField
import           Data.Aeson
import           Data.Attoparsec.Number ( Number(..) )
import           Data.Text
import           Data.Maybe
import           Data.Ratio
import qualified Data.Text as T
import           Money
import           Debug.Trace
import qualified Data.ByteString.Char8 as BS
import           Foreign.C.Types
import           Data.Int

data YesNo = Yes | No deriving (Show, Eq)

instance HasSqlValueSyntax be Bool => HasSqlValueSyntax be YesNo where
  sqlValueSyntax Yes = sqlValueSyntax True
  sqlValueSyntax No = sqlValueSyntax False

instance HasSqlValueSyntax be Int64 => HasSqlValueSyntax be CTime where
  sqlValueSyntax (CTime num) = sqlValueSyntax num

instance FromField CTime where
  fromField f mbs = do
    case BS.unpack `fmap` mbs of
      Nothing -> returnError UnexpectedNull f ""
      Just dat -> return $ CTime $ round $ read $ dat

instance FromBackendRow Postgres CTime

instance HasSqlValueSyntax be String => HasSqlValueSyntax be (Dense "USD") where
  sqlValueSyntax = sqlValueSyntax . toString
    where
      toString = T.unpack . fromJust . (denseToDecimal Round False Nothing '.' 10 (1 % 1))

instance ToJSON (Dense "USD") where
  toJSON = toJSON . fromJust . toText
    where
      toText = denseToDecimal Round False Nothing '.' 6 (1 % 1)

instance FromJSON (Dense "USD") where
  parseJSON = withText "Dense USD" $ \v ->
    return $ fromJust $ denseFromDecimal Nothing '.' (1 % 1) v

instance FromJSON YesNo where
    parseJSON = withNumber "YesNo" parseNum
      where
        parseNum (I n) = if n == 0 then return No else return Yes
        parseNum (D n) = if n == 0 then return No else return Yes

instance ToField YesNo where
    toField Yes = toField (1 :: Int)
    toField No = toField (0 :: Int)

instance FromJSON PgMoney where
  parseJSON v = let parsed = (fromJSON v) :: Result String
                 in case parsed of
                      Error s -> fail s
                      Success a -> return $ PgMoney $ BS.pack a

instance ToJSON PgMoney where
  toJSON (PgMoney repr) = toJSON $ BS.unpack repr
