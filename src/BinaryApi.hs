{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverlappingInstances #-}
{-# LANGUAGE TemplateHaskell #-}

module BinaryApi (
                   module BinaryApi
                 , module BinaryApi.Core
                 ) where

import           BinaryApi.Core
import           Data.Text hiding (map)
import           Data.Aeson
import           Data.Binary.Builder
import           Data.Maybe
import           Data.Ratio
import           Database.Beam.Postgres
import           Data.Aeson.Types
import qualified Data.HashMap.Strict as HMS
import qualified Data.Vector as V
import           GHC.Generics
import           Conversions
import           System.Posix.Types
import           Types
import           Database
import           Control.Lens

-- | History Logs

data HistoryLogsRequest =
    HistoryLogsRequest {
                          _historyLogsRequestTicksHistory :: Text
                        , _historyLogsRequestStart :: EpochTime
                        , _historyLogsRequestEnd :: EpochTime
                        , _historyLogsRequestStyle :: String
                        , _historyLogsRequestCount :: Int
                       }
    deriving (Generic, Show)

makeLenses ''HistoryLogsRequest

defaultHistoryLogsRequest = HistoryLogsRequest {
                              _historyLogsRequestTicksHistory = "R_50"
                            , _historyLogsRequestStart = 0
                            , _historyLogsRequestEnd = 100000
                            , _historyLogsRequestStyle = "candles"
                            , _historyLogsRequestCount = 100000000
                            }

instance ToJSON HistoryLogsRequest where
    toJSON = genericToJSON $ defaultJsonOptions "_historyLogsRequest"
    toEncoding = genericToEncoding $ defaultJsonOptions "_historyLogsRequest"

data HistoryLogsResponse = HistoryLogsResponse [ Candle ]
    deriving (Generic, Show)

instance FromJSON HistoryLogsResponse where
    parseJSON json = do
        list <- withObject "HistoryLogsResponse" (\v -> return $ HMS.lookupDefault (Array V.empty) "candles" v) json
        logs <- parseJSONList list
        return $ HistoryLogsResponse logs

instance BinaryApi HistoryLogsRequest HistoryLogsResponse where
  parseResponse request value =
    case fromJSON value of
      Error msg -> fail msg
      Success (HistoryLogsResponse candles) -> return $ HistoryLogsResponse $ map addSymbol candles
    where
      addSymbol candle = candle { _candleSymbol = SymbolId symbolCode }
      symbolCode = _historyLogsRequestTicksHistory request

-- | Symbols

data ActiveSymbolsRequest =
    ActiveSymbolsRequest {
                            _activeSymbolsRequestActiveSymbols :: Text
                         ,  _activeSymbolsRequestProductType :: Text
                         }
    deriving (Generic, Show)

defaultActiveSymbolsRequest = ActiveSymbolsRequest {
                                                      _activeSymbolsRequestActiveSymbols = "brief"
                                                   ,  _activeSymbolsRequestProductType = "basic"
                                                   }

instance ToJSON ActiveSymbolsRequest where
    toJSON = genericToJSON $ defaultJsonOptions "_activeSymbolsRequest"
    toEncoding = genericToEncoding $ defaultJsonOptions "_activeSymbolsRequest"

data ActiveSymbolsResponse = ActiveSymbolsResponse [ Symbol ]
  deriving (Generic, Show)

instance FromJSON ActiveSymbolsResponse where
    parseJSON json = do
        list <- withObject "ActiveSymbolsResponse" (\v -> return $ HMS.lookupDefault (Array V.empty) "active_symbols" v) json
        symbols <- parseJSONList list
        return $ ActiveSymbolsResponse symbols

instance BinaryApi ActiveSymbolsRequest ActiveSymbolsResponse
