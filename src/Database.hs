{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}

module Database ( module Database
                , module Database.Beam.Query
                , module Database.Beam.Postgres
                , module Database.Beam.Postgres.Full
                , module Database.Beam.Schema
                ) where

import           Database.Beam
import           Database.Beam.Backend.SQL
import           Database.Beam.Postgres
import           Database.Beam.Postgres.Full
import           Database.Beam.Schema
import           Database.Beam.Query hiding (runInsert, insert)
import           Database.Core
import           Database.PostgreSQL.Simple.ToField
import           Data.Aeson
import           Control.Lens
import           Data.Attoparsec.Number ( Number(..) )
import           Data.Text
import           System.Posix.Types
import           Money
import           Types

-- | symbols

data SymbolT f =
  Symbol
  { _symbolCode :: C f Text
  , _symbolAllowForwardStarting :: C f YesNo
  , _symbolType :: C f Text
  , _symbolDisplayName :: C f Text
  , _symbolIsTradingSuspended :: C f YesNo
  , _symbolExchangeIsOpen :: C f YesNo
  , _symbolMarket :: C f Text
  , _symbolSubmarket :: C f Text
  , _symbolPip :: C f (Dense "USD")
  }
  deriving Generic

makeLenses ''SymbolT

type Symbol = SymbolT Identity
type SymbolId = PrimaryKey SymbolT Identity

deriving instance Show Symbol
deriving instance Show (PrimaryKey SymbolT Identity)
deriving instance Eq Symbol
deriving instance Eq (PrimaryKey SymbolT Identity)

instance Beamable SymbolT
instance Beamable (PrimaryKey SymbolT)

instance Table SymbolT where
  data PrimaryKey SymbolT f = SymbolId (C f Text) deriving Generic
  primaryKey = SymbolId . _symbolCode

instance FromJSON Symbol where
    parseJSON = withObject "Symbol" $ \v ->
      Symbol <$> v .: "symbol" <*>
                 v .: "allow_forward_starting" <*>
                 v .: "symbol_type" <*>
                 v .: "display_name" <*>
                 v .: "is_trading_suspended" <*>
                 v .: "exchange_is_open" <*>
                 v .: "market" <*>
                 v .: "submarket" <*>
                 v .: "pip"

-- | candles

data CandleT f =
  Candle
  {
    _candleEpoch :: C f EpochTime
  , _candleOpen :: C (Nullable f) (Dense "USD")
  , _candleHigh :: C (Nullable f) (Dense "USD")
  , _candleLow :: C (Nullable f) (Dense "USD")
  , _candleClose :: C (Nullable f) (Dense "USD")
  , _candleIsValid :: C f Bool
  , _candleSymbol :: PrimaryKey SymbolT f
  }
  deriving Generic

makeLenses ''CandleT

type Candle = CandleT Identity

deriving instance Show Candle
deriving instance Eq Candle

instance Beamable CandleT
instance Beamable (PrimaryKey CandleT)

instance Table CandleT where
  data PrimaryKey CandleT f = CandleId (PrimaryKey SymbolT f) (C f EpochTime) deriving Generic
  primaryKey = CandleId <$> _candleSymbol <*> _candleEpoch

defaultInvalidCandle :: CandleT Identity
defaultInvalidCandle = Candle 0 Nothing Nothing Nothing Nothing False (SymbolId "")

instance FromJSON Candle where
    parseJSON = withObject "Candle" $ \v ->
      Candle <$> v .:  "epoch" <*>
                 v .:? "open" <*>
                 v .:? "high" <*>
                 v .:? "low" <*>
                 v .:? "close" <*>
                 (return True) <*>
                 (return $ SymbolId "")

-- views:

data LastYearCandleDownloadT f =
  LastYearCandleDownload
  {
    _lastYearCandleDownloadEpoch :: C f EpochTime
  , _lastYearCandleDownloadCode :: C f Text
  }
  deriving Generic

type LastYearCandleDownload = LastYearCandleDownloadT Identity

makeLenses ''LastYearCandleDownloadT

deriving instance Show LastYearCandleDownload
deriving instance Eq LastYearCandleDownload

instance Beamable LastYearCandleDownloadT
instance Beamable (PrimaryKey LastYearCandleDownloadT)

instance Table LastYearCandleDownloadT where
  data PrimaryKey LastYearCandleDownloadT f = LastYearCandleDownloadId deriving Generic
  primaryKey _ = LastYearCandleDownloadId

-- Database:

data KupiecDb f =
  KupiecDb
  { _kupiecSymbols :: f (TableEntity SymbolT)
  , _kupiecCandles :: f (TableEntity CandleT)
  , _kupiecLastYearCandleDownloads :: f (TableEntity LastYearCandleDownloadT)
  }
  deriving Generic

makeLenses ''KupiecDb

instance Database be KupiecDb

kupiecDb :: DatabaseSettings be KupiecDb
kupiecDb = defaultDbSettings `withDbModification`
  dbModification {
                   _kupiecLastYearCandleDownloads =
                     modifyTable (\_ -> "last_year_candle_downloads") $
                       tableModification {
                                           _lastYearCandleDownloadEpoch = fieldNamed "epoch"
                                         , _lastYearCandleDownloadCode = fieldNamed "code"
                                         }
                 }


