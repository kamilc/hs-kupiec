{ mkDerivation, aeson, base, bytestring, dotenv, MissingH, mtl
, postgresql-simple, postgresql-simple-migration, resource-pool
, safe-money, stdenv, stm, text, unordered-containers, utf8-string
, websockets
}:
mkDerivation {
  pname = "hs-kupiec";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base bytestring dotenv MissingH mtl postgresql-simple
    postgresql-simple-migration resource-pool safe-money stm text
    unordered-containers utf8-string websockets
  ];
  executableHaskellDepends = [
    aeson base bytestring dotenv MissingH mtl postgresql-simple
    postgresql-simple-migration resource-pool safe-money stm text
    unordered-containers utf8-string websockets
  ];
  testHaskellDepends = [
    aeson base bytestring dotenv MissingH mtl postgresql-simple
    postgresql-simple-migration resource-pool safe-money stm text
    unordered-containers utf8-string websockets
  ];
  homepage = "https://github.com/githubuser/hs-kupiec#readme";
  license = stdenv.lib.licenses.bsd3;
}
